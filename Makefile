.SILENT:
.PHONY: docs format lint test watch run dist escript

## help: this help file
help:
	@( echo "" && echo "Makefile targets..." && echo "" )
	@( cat Makefile | grep '^##' | sed -e 's/##/ -/' | sort && echo "" )

## format: run the mix formatter
format:
	mix format --check-formatted && echo "format is good..." || mix format

## docs: build the docs
docs:
	mix docs

## run: run in iex
run:
	iex -S mix

## deps: fetch the mix deps
deps:
	mix deps.get

## credo: run the credo/linter report
credo:
	mix credo --all

## test: test the application
test:
	mix test --cover

## watch: watch test and lib files and compile and test on change
watch:
	clear
	while true; do inotifywait -r -e modify --exclude=".swp" lib test config; clear; make test; done

