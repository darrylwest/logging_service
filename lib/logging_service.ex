defmodule LoggingService do
  @moduledoc """
  Documentation for `LoggingService`.
  """

  @doc """
  Return the current module version (not set in mix)
  """
  def version() do
    {:ok, vsn} = :application.get_key(:logging_service, :vsn)
    List.to_string(vsn)
  end
end
