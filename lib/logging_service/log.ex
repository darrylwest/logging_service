defmodule LoggingService.Log do
  @moduledoc """
  LoggingService with ets backing via.  Uses erlang standard logger.

  Standard template: `"[~w:~w] ~s ~w"`.  This template expects a module, line number text message
  and paramers.

  ## Application Config

    children = [
      {LoggingService.Log, [level: :info]}
    ]

  ## Example Use:

    alias LoggingService.Log

    {_, user} = User.find(id)
    Log.info_with_template(__MODULE__, __ENV__.line, "user: ", user)
    18:16:45.356 [info] [User:344] user: {"1J4DW1T9H0KXYQEWIWJRSP", ...}

  """

  @name :application_logger

  use GenServer

  ################################################################################# Public API

  @doc "emit a debug message with optional formatting"
  def debug(format, args \\ []) when is_list(args), do: log(@name, :debug, format, args)

  @doc "emit an info message with optional formatting"
  def info(format, args \\ []) when is_list(args), do: log(@name, :info, format, args)

  @doc "emit a notice message with optional formatting"
  def notice(format, args \\ []) when is_list(args), do: log(@name, :notice, format, args)

  @doc "emit a warning message"
  def warn(format, args) when is_list(args), do: log(@name, :warning, format, args)

  @doc "emit an error message"
  def error(format, args) when is_list(args), do: log(@name, :error, format, args)

  @doc """
  Logs a standard info message with the standard template.
  """
  def info_with_template(mod, line, description, err) do
    log(@name, :info, template(), [mod, line, description, err])
  end

  @doc """
  Logs a standard notice message.
  """
  def notice_with_template(mod, line, description, err) do
    log(@name, :notice, template(), [mod, line, description, err])
  end

  @doc """
  Logs a standard warning.
  """
  def warn_with_template(mod, line, description, err) do
    log(@name, :warning, template(), [mod, line, description, err])
  end

  @doc """
  Logs a standard error using the standard template of module, line, description, term.

  ## Example

    Log.error_with_template(__MODULE__, __ENV__.line, "my error", error_term)

  """
  def error_with_template(mod, line, description, err) do
    log(@name, :error, template(), [mod, line, description, err])
  end

  @doc """
  Returns the default template.  Typical use.

  ## Example Use:

    Log.template() |> Log.info([__MODULE__, __ENV__.line, "my user: ", User.random()])

  """
  def template(), do: "[~w:~w] ~s ~w"

  defp log(name, level, format, args) do
    lvl = :logger.get_primary_config()[:level]

    case :logger.compare_levels(lvl, level) do
      :gt -> :ok
      _ -> GenServer.cast(name, {:log, level, format, args})
    end
  end

  @doc """
  Returns the list of log messages sorted by date to return the most recent entries last and trimmed to
  the specified count defaulting to 100.  For example, `Log.list(10)` would list the last 10 log enties
  with the newest entry at the bottom of the list, similar to how a `tail` operation would run.
  """
  def list(count \\ 100) do
    GenServer.call(@name, :list)
    |> Enum.sort_by(fn {id, _} -> id * -1 end)
    |> Enum.map(fn {id, v} -> {id_to_date(id), v} end)
    |> Enum.take(count)
    |> Enum.reverse()
  end

  @doc "Returns the error messages upto the specified count."
  def errors(count \\ 1_000), do: filter(:error, count)

  @doc "Returns the warning messages upto the specified count."
  def warnings(count \\ 1_000), do: filter(:warning, count)

  def stats() do
    map = %{error: 0, info: 5, notice: 0, warning: 0, debug: 0}
    GenServer.call(@name, :list)
    |> Enum.reduce(map, fn {_, {lvl, _, _}}, acc -> Map.put(acc, lvl, Map.get(acc, lvl, 0) + 1) end)
  end

  @doc "Returns the error messages upto the specified count."
  def filter(level, count \\ 1_000) do
    GenServer.call(@name, :list)
    |> Enum.filter(fn {_, {lvl, _, _}} -> lvl == level end)
    |> Enum.sort_by(fn {id, _} -> id * -1 end)
    |> Enum.map(fn {id, v} -> {id_to_date(id), v} end)
    |> Enum.take(count)
    |> Enum.reverse()
  end

  @doc "Returns the total number of errors currently in the log."
  def total_errors() do
    GenServer.call(@name, :list)
    |> Enum.filter(fn {_, {level, _, _}} -> level == :error end)
    |> Enum.count()
  end

  @doc "convert the standard timestamp key to a readable date/time.  This process looses resoltion."
  def id_to_date(id) do
    micros = rem(id, 1_000_000_000) |> div(1_000)
    :calendar.system_time_to_universal_time(id, :native)
    |> NaiveDateTime.from_erl!({micros, 6})
    |> NaiveDateTime.to_iso8601()
  end

  @doc "Set the log level to the specifed value."
  def set_level(level) when level in [:debug, :info, :notice, :warning, :error] do
    :logger.get_primary_config() |> Map.put(:level, level) |> :logger.set_primary_config()
  end

  @doc "return the current log level"
  def get_level(), do: :logger.get_primary_config() |> Map.get(:level)

  @doc "returnt the total number of messages"
  def count(), do: GenServer.call(@name, :count)

  @doc "return info about this log server"
  def info(), do: GenServer.call(@name, :info)

  @doc "Calculate the uptime and return the total seconds, total minutes, total hours and total days"
  def calc_uptime(start_time) do
    now = NaiveDateTime.utc_now()
    seconds = NaiveDateTime.diff(now, start_time)
    %{
      seconds: seconds,
      minutes: Float.round(seconds / 60, 2),
      hours: Float.round(seconds / (60 * 60), 2),
      days: Float.round(seconds / (60 * 60 * 24), 2),
    }
  end

  @doc """
  Trim the debug, info and notice messages to leave the most recent messages at the specified count.  This
  should be invoked from time to time to keep the log list within a reasonable length while retaining the
  warn and error messages.
  """
  def trim(count \\ 200) do
    list =
      GenServer.call(@name, :list)
      |> Enum.filter(fn {_, {lvl, _, _}} -> :logger.compare_levels(lvl, :notice) in [:lt, :eq] end)
      |> Enum.sort_by(fn {id, _} -> id end)

    target = (length(list) - count) |> max(0)

    removed = list |> Enum.take(target) |> Enum.map(fn {id, _} -> GenServer.call(@name, {:remove, id}) end)

    {:ok, length(removed)}
  end

  @doc """
  Clear all log entries at or below the specified level.  Use this from time to time to trim out all the old
  messages at or below the specified level.
  """
  def clear(level) when level in [:debug, :info, :notice, :warning, :error] do
    list =
      GenServer.call(@name, :list)
      |> Enum.filter(fn {_, {lvl, _, _}} -> :logger.compare_levels(lvl, level) in [:lt, :eq] end)
      |> Enum.map(fn {id, _} -> GenServer.call(@name, {:remove, id}) end)

    {:ok, length(list)}
  end

  @doc "Clears all the messages"
  def clear_all(), do: GenServer.call(@name, :clear_all)

  ################################################################################# GenServer Callbacks

  def start_link(opts) do
    name = opts[:name] || @name
    state = create_state(name, opts)

    GenServer.start_link(__MODULE__, state, name: name)
  end

  def create_state(name, opts) do
    level = opts[:level] || :info
    :logger.get_primary_config() |> Map.put(:level, level) |> :logger.set_primary_config()

    %{
      name: name,
      options: opts,
      filename: opts[:filename] || "/opt/application-log.term",
      db: nil,
      start_time: nil,
      last_saved: nil,
      trim_tref: nil,
      auto_trim_to: opts[:auto_trim_to] || 5_000
    }
  end

  @impl true
  def init(state) do
    state =
      state
      |> Map.put(:db, create_new(state))
      |> Map.put(:start_time, NaiveDateTime.utc_now())

    if state.auto_trim_to > 0 do
      interval = 10 * 60 * 1000 # 10 minutes
      {:ok, tref} = :timer.apply_interval(interval, LoggingService.Log, :trim, [state.auto_trim_to])

      {:ok, Map.put(state, :trim_tref, tref)}
    else
      {:ok, state}
    end
  end

  defp create_new(state) do
    opts = [
      :set,
      :protected,
      :named_table,
      write_concurrency: false,
      read_concurrency: false
    ]

    :ets.new(state.name, opts)
  end

  @impl true
  def handle_call(:list, _from, state) do
    list = :ets.tab2list(state.db)

    {:reply, list, state}
  end

  @impl true
  def handle_call({:remove, key}, _from, state) do
    :ets.delete(state.db, key)
    {:reply, :ok, state}
  end

  @impl true
  def handle_call(:clear_all, _from, state) do
    :ets.delete_all_objects(state.db)
    {:reply, :ok, state}
  end

  @impl true
  def handle_call(:count, _from, state) do
    {:reply, :ets.info(state.db, :size), state}
  end

  @impl true
  def handle_call(:info, _from, state) do
    reply = [
      name: state.name,
      pid: self(),
      db: :ets.info(state.db),
      level: :logger.get_primary_config()[:level],
      options: state.options,
      auto_trim_to: state.auto_trim_to,
      trim_tref: state.trim_tref,
      start_time: state.start_time,
      uptime: calc_uptime(state.start_time),
      filename: state.filename,
      last_saved: state.last_saved
    ]

    {:reply, reply, state}
  end

  @impl true
  def handle_cast({:log, level, format, args}, state) do
    :logger.log(level, format, args)
    :ets.insert(state.db, {:os.system_time(:nanosecond), {level, format, args}})

    {:noreply, state}
  end
end
