defmodule LoggingServiceLogTest do
  use ExUnit.Case
  doctest LoggingService.Log

  alias LoggingService.Log

  setup_all do
    Log.set_level(:info)
  end

  describe "logging.log" do
    test "debug" do
      Log.set_level(:info)
      count = Log.count()
      assert Log.debug("this is a test")
      assert Log.count() == count
    end

    test "info" do
      count = Log.count()
      assert Log.info("this is an info test")
      Process.sleep(10)
      assert Log.count() == count + 1
    end

    test "notice" do
      count = Log.count()
      assert Log.notice("this is an notice test")
      Process.sleep(10)
      assert Log.count() == count + 1
    end

    test "warn" do
      count = Log.count()
      assert Log.warn("this is an warning test", [])
      Process.sleep(10)
      assert Log.count() == count + 1
    end

    test "error" do
      count = Log.count()
      assert Log.error("this is an error test", [])
      Process.sleep(10)
      assert Log.count() == count + 1
    end

    test "info_with_template/4" do
      assert :ok = Log.info_with_template(__MODULE__, __ENV__.line, "my test info", reason: "bad params")
    end

    test "notice_with_template/4" do
      assert :ok = Log.notice_with_template(__MODULE__, __ENV__.line, "my test notice", reason: "bad params")
    end

    test "warn_with_template/4" do
      assert :ok = Log.warn_with_template(__MODULE__, __ENV__.line, "my test warning", reason: "bad params")
    end

    test "error_with_template/4" do
      assert :ok = Log.error_with_template(__MODULE__, __ENV__.line, "my test error", reason: "bad params")
    end

    test "list" do
      for n <- 1..80, do: Log.error("error ~w", [n + 5000])
      for n <- 1..80, do: Log.warn("warning ~w", [n + 4000])
      for n <- 1..80, do: Log.notice("notice ~w", [n + 3000])
      assert Log.list(240) |> length() == 240
      count = Log.count()
      assert Log.list(count + 100) |> length() == count
      assert Log.list() |> length() == 100
    end

    test "errors" do
      for n <- 1..20, do: Log.error("error ~w", [n + 5000])
      assert Log.errors() |> length() >= 20
      assert Log.errors(100) |> length() >= 20
    end

    test "total_errors" do
      for n <- 1..20, do: Log.error("error ~w", [n + 5000])
      assert Log.total_errors() >= 20
    end

    test "warnings" do
      for n <- 1..30, do: Log.warn("warn ~w", [n + 4000])
      assert Log.warnings() |> length() >= 30
      assert Log.warnings(100) |> length() >= 30
    end

    test "filter/2" do
      for n <- 1..30, do: Log.info("info ~w", [n + 1000])
      assert Log.filter(:info) |> length() >= 30
      assert Log.filter(:info, 250) |> length() >= 30
    end

    test "get_level/0" do
      assert Log.get_level() in [:debug, :info, :notice, :warning]
    end

    test "set_level/1" do
      level = Log.get_level()
      assert level in [:debug, :info, :notice, :warning, :error]
      assert Log.set_level(level)
    end

    test "info/0" do
      assert info = Log.info()
      assert is_list(info)
      assert info[:level] == Log.get_level()
    end

    test "trim/0" do
      for n <- 1..100, do: Log.info("my info test: ~w", [n])
      Process.sleep(100)
      assert {:ok, n} = Log.trim(50)
      assert n >= 50
      assert {:ok, n} = Log.trim()
      assert n >= 0
    end

    test "clear/1" do
      for _ <- 1..10 do
        Log.debug("debug")
        Log.info("info")
        Log.notice("notice thing", [])
        Log.warn("a warning", [])
        Log.error("ERROR!!!", [])
      end

      Process.sleep(50)
      count = Log.count()
      assert {:ok, n} = Log.clear(:warning)
      assert count > n
      assert Log.count() >= 10
    end

    test "stats" do
      Log.set_level(:debug)
      for n <- 1..20, do: Log.error("error ~w", [n + 5000])
      for n <- 1..30, do: Log.warn("warning ~w", [n + 4000])
      for n <- 1..40, do: Log.notice("notice ~w", [n + 3000])
      for n <- 1..50, do: Log.info("notice ~w", [n + 2000])
      for n <- 1..10, do: Log.debug("notice ~w", [n + 1000])
      assert stats = Log.stats()
      assert stats.error >= 20
      assert stats.warning >= 30
      assert stats.notice >= 40
      assert stats.info >= 50
      assert stats.debug >= 10
    end

    test "clear_all/0" do
      assert :ok == Log.clear_all()
    end
  end
end
