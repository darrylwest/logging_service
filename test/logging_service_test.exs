defmodule LoggingServiceTest do
  use ExUnit.Case
  doctest LoggingService

  describe "logging service" do
    test "version" do
      version = LoggingService.version()
      assert version =~ ~r/^\d+\.\d+\.\d+$/
    end
  end
end
