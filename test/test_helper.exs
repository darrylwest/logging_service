#
# could use the Fixtures.exs script if it is pre-compiled and put in _build/test/...
#

LoggingService.Log.start_link(level: :info)

ExUnit.configure(capture_log: true)
ExUnit.start()
